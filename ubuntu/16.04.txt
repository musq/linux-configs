1. Overheating issue

https://itsfoss.com/reduce-overheating-laptops-linux/

sudo add-apt-repository ppa:linrunner/tlp
sudo apt-get update
sudo apt-get install tlp tlp-rdw
sudo reboot

Configure the tlp-config as shown in the documentation

2. Install redshift
sudo apt-get install redshift redshift-gtk

3. Install pip
sudo apt install python-pip

=> Upgrade pip
sudo -H pip install --upgrade pip

4. Install pip3
sudo apt install python3-pip

=> Upgrade pip3
sudo -H pip3 install --upgrade pip

5. Install psensor
search online

6. Set launcher icon size to 50 in Appearance

7. Set firefox Minimum Font Size in General > Appearance to 16
Block more notifications from websites in Notifications settings

#### Firefox (about:config)
general.smoothScroll.mouseWheel.durationMaxMS;180

8. Install git
sudo apt-get install git

9. Install virtualenv
pip3 install virtualenv

10. Install venv
sudo apt-get install python3-venv

11. Install gmvault
Set up a new virtualenv with python2
virtualenv --python=/usr/bin/python env
source env/bin/activate
pip install gmvault

12. Allow multitouch
https://github.com/bulletmark/libinput-gestures

13. Throttle CPU to save power (not sure if this actually works!)
https://github.com/Sepero/temp-throttle

14. Disable Ctrl+scroll to zoom pages in firefox
about:config
mousewheel.with_control.action = 1

15. Remove delay in scroll in firefox
general.smoothScroll.durationToIntervalRatio = 1

16. Install arduino
Download and unpack arduino 64 bit for linux
DO NOT RUN install.sh
Copy lib/desktoptemplate to ~/Desktop/arduino-arduinoide.desktop
Replace the following:
Exec=/home/bro/Documents/arduino/v1.8.5/arduino
Icon=/home/bro/Documents/arduino/v1.8.5/lib/arduino_icon.ico
Run
chmod +x arduino-arduinoide.desktop

17. Enable palm rejection
https://askubuntu.com/questions/931761/how-to-fix-palm-rejection-on-ubuntu-16-04-lts

First, you want to find your touch pad driver. You can do so by typing under terminal:
```xinput```
There you will receive a list of drivers. Under list Virtual core pointer, look for keyword TouchPad. That variable will be the {id} e.g. mine is "SynPS/2 Synaptics TouchPad".
Next go to Startup Applications Preferences, select "Add", you can put whatever for the name, I used "Palm Detection", and for the cmd you want to put:
```xinput set-prop "{id}" "Synaptics Palm Detection" 1```
Also, add another one to set the dimensions. Call it "Palm Dimensions" (you can replace the value as you wish, you might want to play with the value to get the best experience):
```xinput set-prop "{id}" "Synaptics Palm Dimensions" 7, 7```

18. Map CapsLock key to Ctrl
https://www.emacswiki.org/emacs/MovingTheCtrlKey#toc9

To make Caps Lock another Ctrl key, edit the file /etc/default/keyboard and change the line which reads
 XKBOPTIONS=""
to
 XKBOPTIONS="ctrl:nocaps"		# Some people prefer "ctrl:swapcaps"
and then run:
```sudo dpkg-reconfigure -phigh console-setup```





